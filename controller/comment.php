<?php

class comment
{
    function allForProduct($Error = '')
    {
        if(isset($_GET["product_id"]))
            $product_id = $_GET["product_id"];

        $comments = comment_model::allForProduct($product_id);

        require_once("model/product.php");
        $product = product_model::get($product_id);

        require_once("view/comment/index.php");
    }

    function delete()
    {
        if(isset($_GET["id"]))
        $id = $_GET["id"];
        $product_id = comment_model::getComment($id)->product_id;

        $comment = comment_model::delete($id);

        require_once("view/comment/deleted.php");
    }

    function add()
    {
        if(isset($_GET["product_id"])) {
            $nickname = $_POST["nickname"];
            $message = $_POST["message"];
            $rating = $_POST["rating"];
            if ($rating < 0 || $rating > 10)
                return $this->allForProduct($Error = 'Please enter a number between 0 and 10');
            $product_id = $_GET["product_id"];
            $created_at = date("Y-m-d H:i:s");

            $comment = comment_model::add($nickname, $message, $rating, $product_id, $created_at);

            require_once("view/comment/added.php");
        }
    }
}