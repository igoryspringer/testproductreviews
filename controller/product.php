<?php

require_once 'vendor/abeautifulsite/simpleimage/src/claviska/SimpleImage.php';

class product
{
    const DIR_IMG = '/home/igorspringer/projects/www/testProductReviews/image/';

    function all()
    {
        $products=product_model::all();

        require_once("view/product/index.php");
    }

    function delete(){
        if(isset($_GET["id"])) {
            $id = $_GET["id"];

            $product = product_model::get($id);
            if (file_exists(self::DIR_IMG . $product->name . '.png'))
                unlink(self::DIR_IMG . $product->name . '.png');
            if (file_exists(self::DIR_IMG . $product->name . '_thumb.png'))
                unlink(self::DIR_IMG . $product->name . '_thumb.png');

            $product = product_model::delete($id);

            require_once("view/product/deleted.php");
        }
    }

    function add()
    {
        if($_POST && $_FILES) {
            $name = $_POST["name"];
            $nickname = $_POST["nickname"];
            $price = $_POST["price"];
            $created_at = date("Y-m-d H:i:s");

            $file = self::DIR_IMG . $name . '.png';
            $image = '/image/' . $name . '.png';
            move_uploaded_file($_FILES['image']['tmp_name'], $file);

            try {
                $image_thumb = new \claviska\SimpleImage();
                $image_thumb
                    ->fromFile(self::DIR_IMG . $name . '.png')
                    ->resize(100, 50)
                    ->toFile(self::DIR_IMG . $name . '_thumb.png', 'image/png');
            } catch(Exception $err) {
                echo $err->getMessage();
            }
            $image_thumb = '/image/' . $name . '_thumb.png';

            $product = product_model::add($name, $image, $image_thumb, $created_at, $nickname, $price);

            require_once("view/product/added.php");
            return;
        }

        require_once("view/product/add.php");
    }
}
