<?php

class comment_model
{
    public $id;
    public $nickname;
    public $message;
    public $rating;
    public $product_id;
    public $created_at;
    public $averageRating;

    public function __construct($id, $nickname, $message, $rating, $product_id, $created_at, $averageRating = 0) {
          $this->id = $id;
          $this->nickname = $nickname;
          $this->message = $message;
          $this->rating  = $rating;
          $this->product_id  = $product_id;
          $this->created_at  = $created_at;
          $this->averageRating  = $averageRating;
    }

    public static function getComment($id)
    {
        $list = [];
        $db = Db::getInstance();
        $result = mysqli_query($db,"SELECT * FROM comment where id = $id");
        $averageRating = null;

        while($row = mysqli_fetch_assoc($result)){
            $list = new comment_model($row['id'], $row['nickname'], $row['message'], $row['rating'], $row['product_id'], $row['created_at'], $row['averageRating']);
        }

        return $list;
    }

    public static function allForProduct($product_id)
    {
        $list = [];
        $db = Db::getInstance();
        if($result = mysqli_query($db,"SELECT *, (SELECT AVG(rating) FROM comment WHERE product_id = $product_id) AS averageRating FROM comment where product_id = $product_id")) {
            while($row = mysqli_fetch_assoc($result)){
                $list[] = new comment_model($row['id'], $row['nickname'], $row['message'], $row['rating'], $row['product_id'], $row['created_at'], $row['averageRating']);
            }
        }

        return $list;
    }

    public static function delete($id)
    {

        $db = Db::getInstance();
        $result = mysqli_query($db,"delete from comment where id='$id'");

        return true;
    }

    public static function add($nickname, $message, $rating, $product_id, $created_at) {

        $db = Db::getInstance();
        $result = mysqli_query($db,"Insert into comment (nickname,message,rating,product_id,created_at) Values ('$nickname','$message','$rating','$product_id', '$created_at')");
        $id = mysqli_insert_id($db);
        $averageRating = null;

        return new comment_model($id, $nickname, $message, $rating, $product_id, $created_at, $averageRating);
    }
}
