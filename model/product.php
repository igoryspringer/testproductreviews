<?php

class product_model
{
    public $id;
    public $name;
    public $image;
    public $image_thumb;
    public $created_at;
    public $nickname;
    public $commentsCount;
    public $price;

    public function __construct($id, $name, $image, $image_thumb, $created_at, $nickname, $commentsCount, $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->image = $image;
        $this->image_thumb = $image_thumb;
        $this->created_at = $created_at;
        $this->nickname = $nickname;
        $this->commentsCount = $commentsCount;
        $this->price = $price;
    }

    public static function get($product_id)
    {
        $list = [];
        $db = Db::getInstance();

        if($result = mysqli_query($db,"SELECT * FROM product where id = $product_id")) {
            if($row = mysqli_fetch_assoc($result)){
                $list = new product_model($row['id'], $row['name'], $row['image'], $row['image_thumb'], $row['created_at'], $row['nickname'], $row['commentsCount'], $row['price']);
            }
        }

        return $list;
    }

    public static function all()
    {
        $list = [];
        $db = Db::getInstance();
        $result = mysqli_query($db,'SELECT *, (SELECT COUNT(DISTINCT c.id) FROM comment c WHERE c.product_id = p.id) AS commentsCount FROM product p');

        while($row = mysqli_fetch_assoc($result)){
            $list[] = new product_model($row['id'], $row['name'], $row['image'], $row['image_thumb'], $row['created_at'], $row['nickname'], $row['commentsCount'], $row['price']);
        }

        return $list;
    }

    public static function delete($id)
    {
        $db = Db::getInstance();
        $result = mysqli_query($db,"delete from product where id='$id'");
        require_once("model/comment.php");
        $result2 = mysqli_query($db,"delete from comment where product_id='$id'");

        return true;
    }

    public static function add($name, $image, $image_thumb, $created_at, $nickname, $price)
    {
        $db = Db::getInstance();
        $result = mysqli_query($db,"Insert into product (name, image, image_thumb, created_at, nickname, price) Values ('$name', '$image', '$image_thumb', '$created_at', '$nickname', '$price')");
        $id = mysqli_insert_id($db);
        $commentsCount = null;

        return new product_model($id, $name, $image, $image_thumb, $created_at, $nickname, $commentsCount, $price);
    }
}
