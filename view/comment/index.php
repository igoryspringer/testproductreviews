<?php include "view/header.php"?>
<br>
<br>
<div class="container">
    <h3 style="text-align: center;color: inherit;">Product's comments</h3>
    <br>
    <div class="row">
        <div class="col">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Created at</th>
                    <th>Nickname</th>
                    <th>Price</th>
                    <th>Average rating</th>
                </tr>
                    <tr>
                        <td>
                            <a href="index.php?controller=comment&action=allForProduct&product_id=<?php echo $product->id;?>"><?php echo $product->name;?></a>
                        </td>
                        <td>
                            <img src="<?php echo $product->image;?>" alt="<?php echo $product->name;?>">
                        </td>
                        <td>
                            <?php echo $product->created_at;?>
                        </td>
                        <td>
                            <?php echo $product->nickname;?>
                        </td>
                        <td>
                            <?php echo $product->price;?>
                        </td>
                        <td>
                            <?php foreach ($comments as $comment) {?>
                                <?php echo round($comment->averageRating, 1);?>
                                <?php break;?>
                            <?php }?>
                        </td>
                    </tr>
            </table>
        </div>
        <div>
            <a class="btn btn-danger btn-lg" role="button" aria-pressed="true" title="Delete" href="index.php?controller=product&action=delete&id=<?php echo $product->id;?>">
                -
            </a>
        </div>
    </div>
</div>
<br>
<br>
<div class="container" style="background-color: #dddddd; padding-bottom: 2%; padding-top: 2%;">
    <div class="row">
        <div class="col">
            <form id="commentForm" value="<?php echo $product->id; ?>" action="index.php?controller=comment&action=add&product_id=<?php echo $product->id;?>" method="POST">
                <div class="input-group mb-3">
                    <h6>Add comment to this product</h6>
                </div>
                <div class="input-group mb-3">
                    <input name = "nickname" type="text" class="form-control" placeholder="Username" aria-label="Nickname" aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <input name = "message" type="text" class="form-control" placeholder="Comment" aria-label="Comment" aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <input name = "rating" type="text" class="form-control" placeholder="Rating" aria-label="Rating" aria-describedby="basic-addon1" required>
                    <span class="input-group has-validation text-danger"><?php echo $Error;?></span>
                </div>
                <input class="btn btn-outline-primary" type="submit" value="Submit">
            </form>
        </div>
    </div>
</div>
<br>
<h3 style="text-align: center;">Comments</h3>
<br>
    <div id="comments" class="container">
        <div class="row">
            <div class="col">
                <table>
                    <tr>
                        <th>#</th>
                        <th>Nickname</th>
                        <th>Rating</th>
                        <th>Comment</th>
                        <th>Created at</th>
                        <th>Action</th>
                    </tr>
                    <?php $i=1;?>
                    <?php foreach ($comments as $comment) {?>
                        <tr>
                            <td>
                                <?php echo $i;?>
                            </td>
                            <td>
                                <?php echo $comment->nickname;?>
                            </td>
                            <td>
                                <?php echo $comment->rating;?>
                            </td>
                            <td>
                                <?php echo $comment->message;?>
                            </td>
                            <td>
                                <?php echo $comment->created_at;?>
                            </td>
                            <td>
                                <a class="btn btn-danger btn-lg" role="button" aria-pressed="true" title="Delete" href="index.php?controller=comment&action=delete&id=<?php echo $comment->id;?>">
                                    -
                                </a>
                            </td>
                        </tr>
                    <?php }?>
                </table>
            </div>
        </div>
    </div>
<br>
<br>
<br>
<br>
<br>
<?php include "view/footer.php"?>