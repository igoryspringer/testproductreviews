<!-- Footer -->
<footer class="page-footer font-small" style="
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: #0033A1;
        color: white;
        text-align: center;">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">
            <!-- Grid column -->
            <div class="col">
                <!-- Content -->
                <br>
                <h5 class="text-uppercase" style="text-align: center">Product Review</h5>
                <p style="text-align: center">Web programming - Igor Ochkan</p>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row -->
    </div>
</footer>
<!-- Footer -->
</body>
</html>
