<?php include "view/header.php";?>
<div class="container" style="background-color: #dddddd; padding-bottom: 2%; padding-top: 1%;">
    <h3 style="text-align: center;color: inherit;">Add a new product</h3>
    <br>
    <div class="row">
        <div class="col">
            <form id="productForm" enctype="multipart/form-data" action="index.php?controller=product&action=add" method="POST">
                <div class="input-group mb-3">
                    <input name = "name" type="text" class="form-control" placeholder="Product name" aria-label="Name" aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <input name = "nickname" type="text" class="form-control" placeholder="Nickname" aria-label="Nickname" aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <input name = "price" type="text" class="form-control" placeholder="Price" aria-label="Price" aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <input name="image" type="file" />
                </div>
                <input id="productBtn" class="btn" type="submit" value="Submit">
            </form>
        </div>
    </div>
</div>
<?php include "view/footer.php";?>
