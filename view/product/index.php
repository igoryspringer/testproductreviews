<?php include "view/header.php"?>
<br>
<br>
<div id="products" class="container">
    <h3 style="text-align: center;">Products</h3>
    <br>
    <div class="row">
        <div class="col">
            <table id="products-table">
                <tr>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Created at</th>
                    <th>Nickname</th>
                    <th>Comments number</th>
                    <th>Price</th>
                </tr>
            <?php foreach ($products as $product) {?>
                <tr>
                    <td>
                        <a href="index.php?controller=comment&action=allForProduct&product_id=<?php echo $product->id;?>"><?php echo $product->name;?></a>
                    </td>
                    <td>
                        <img src="<?php echo $product->image_thumb;?>" alt="<?php echo $product->name;?>">
                    </td>
                    <td>
                        <?php echo $product->created_at;?>
                    </td>
                    <td>
                        <?php echo $product->nickname;?>
                    </td>
                    <td>
                        <?php echo $product->commentsCount;?>
                    </td>
                    <td>
                        <?php echo $product->price;?>
                    </td>
                </tr>
            <?php }?>
            </table>
        </div>
        <div>
            <a class="btn btn-primary btn-lg" role="button" aria-pressed="true" title="Add" href="index.php?controller=product&action=add">
                +
            </a>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<?php include "view/footer.php"?>
