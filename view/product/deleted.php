﻿<?php
include "view/header.php";
echo "<h3 style=\"text-align: center;\">Product deleted</h3>";
?>
<div class="container">
    <div class="row">
        <div class="col">
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="index.php?controller=product&action=all">
                        <?php echo "Return to the products";?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php
include "view/footer.php";
?>