Installation:
- composer install

Database [MySQL]:
- product_reviews.sql

Start:
- $ php -S localhost:8000
